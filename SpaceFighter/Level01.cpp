

#include "Level01.h"
#include "BioEnemyShip.h"
#include "BioEnemy2.h"
#include "EnemyBlaster.h"

void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships

	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip2.png");

	const int COUNT = 12;
	const int COUNT2 = 21;


	double xPositions[COUNT] = // For Normal Enemy
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 
		0.7, 0.75, 0.65
	};
	
	double delays[COUNT] = // For Normal Enemy
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 
		3.25, 0.25, 0.05
	};
	double xPositions2[COUNT2] = // For Enemy2
	{
		0.5, 
		0.5, 0.40, 0.60,
		0.50, 0.4, 0.6, 0.30, 0.70, 
		0.5, 0.4, 0.6, 0.30, 0.70, 0.20, 0.80,
		0.1, 0.9, 0.3, 0.7, 0.5
	};

	double delays2[COUNT2] = // For Enemy2
	{
		4.0, 
		4.0, 1.0, 0.0, 
		6.0, 1.0, 0.0, 1.0, 0.0, 
		6.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0,
		2.0, 0.0, 1.0, 0.0, 5.0
	};


	float delay = 3.0; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);
		
		BioEnemyShip* pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);


	}
	for (int i = 0; i < COUNT2; i++) {
		delay += delays2[i];
		position.Set(xPositions2[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		//Definds enemy 2

		BioEnemy2* pEnemy2 = new BioEnemy2();
		pEnemy2->SetTexture(pTexture);
		pEnemy2->SetCurrentLevel(this);
		pEnemy2->Initialize(position, (float)delay);

		//Sets up weapons for pEnemy2

		EnemyBlaster* pBlaster2 = new EnemyBlaster(true);
		pBlaster2->SetProjectilePool(&m_projectiles);
		pEnemy2->AttachWeapon(pBlaster2, Vector2::UNIT_Y * +30);

		for (int i = 0; i < 25; i++)
		{
			Projectile* pProjectile = new Projectile();

			m_projectiles.push_back(pProjectile);
			AddGameObject(pProjectile);
		}
		AddGameObject(pEnemy2);

	}
	Level::LoadContent(pResourceManager);
}

