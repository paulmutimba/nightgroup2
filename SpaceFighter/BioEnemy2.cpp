#include "BioEnemy2.h"

BioEnemy2::BioEnemy2()
{
	SetSpeed(100);
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
	m_MoveSet = (rand() % 35);
}
void BioEnemy2::Update(const GameTime* pGameTime)
{
	if (IsActive())
	{
		FireWeapons(TriggerType::PRIMARY);
		Vector2 position = GetPosition();
		/**/
		if (m_MoveSet < 10) { // Go down
			TranslatePosition(0, GetSpeed() * pGameTime->GetTimeElapsed());
		}
		if ((m_MoveSet >= 10) && (m_MoveSet <= 20)) { // Go left or right
			if (position.Y > ((Game::GetScreenHeight()) / 4)*3) {
				if (position.X > ((Game::GetScreenWidth()) / 2)) {
					TranslatePosition(GetSpeed() * pGameTime->GetTimeElapsed(), 0);
				}
				else {
					TranslatePosition(-GetSpeed() * pGameTime->GetTimeElapsed(), 0);
				}
			}
			else {
				TranslatePosition(0, GetSpeed() * pGameTime->GetTimeElapsed());
			}
		}
		if (m_MoveSet >20) { // go sideways left or right
			if (position.Y > ((Game::GetScreenHeight()) / 2)) {
				if (position.X > ((Game::GetScreenWidth()) / 2)) {
					TranslatePosition(GetSpeed() * pGameTime->GetTimeElapsed(), GetSpeed() * pGameTime->GetTimeElapsed());
				}
				else {
					TranslatePosition(-GetSpeed() * pGameTime->GetTimeElapsed(), GetSpeed() * pGameTime->GetTimeElapsed());
				}
			}else {
				TranslatePosition(0, GetSpeed() * pGameTime->GetTimeElapsed());
			}
		}
		/**/

		if (!IsOnScreen()) {
			Deactivate();
		}
	}
		EnemyShip::Update(pGameTime);
}
void BioEnemy2::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}